#!/usr/bin/env python
#
# Usage: digikeyRetriever.py downloadurl outputfile 
# Supply a "Download table" URL without the page=X parameter.
# The script will download all the pages, and combine them to the output file supplied.
# The page number is detected when the downloaded table contains only the headers (1 line)
#
import sys
import os
import urllib2

# arguments
baseURL = sys.argv [1]
outputFile = sys.argv [2]
tempFile = "digiTemp.csv"

# remove file if exists 
try:
    os.remove (tempFile)
except OSError:
    pass
try:
    os.remove (outputFile)
except OSError:
    pass
    
page = 1

while True:
    # Retrive CSV page
    attempts = 0
    pageURL = baseURL + "&page=" + str(page)
    while attempts < 3:
        try:
            response = urllib2.urlopen(pageURL, timeout = 10)
            content = response.read()
            f = open( tempFile, 'w' )
            f.write( content )
            f.close()
            break
        except urllib2.URLError as e:
            attempts += 1
            print type(e)
    
    if attempts == 3:
        print "Failed to retrieve page " + str(page)
    
    # Check if CSV has no part data (previous page was the last)
    num_lines = sum(1 for line in open(tempFile))
    if num_lines == 1:
        os.remove (tempFile)
        break
    
    # Add data to the output file
    if page == 1:
        # First page is the beginning of the output file
        os.system ("mv " + tempFile + " " + outputFile)
    else:
        # Remove header
        os.system("tail -n +2 " + tempFile + "> " + tempFile + ".tmp && mv " + tempFile + ".tmp " + tempFile)
        # Append file
        os.system("cat " + tempFile + " >> " + outputFile) 
        os.remove (tempFile)
    # Retrieved parts = completedPages * 500 + parts in last page retrieved (which may be less than 500)
    sys.stdout.write('\r' + "Retrieved " + str((page -1) * 500 + (num_lines-1)) + " parts")
    sys.stdout.flush() # important
    page += 1

num_lines = sum(1 for line in open(outputFile))  
print "\rRetrieved " + str(num_lines -1) + " parts"  
