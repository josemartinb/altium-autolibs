import json
import urllib
import contextlib

import time

class Octopart:
    apiQueryCount = 0
    apiTimeStamps = []
    
    def __init__(self, apiKey):
        self.apiKey = apiKey
        self.apiQueryCount = 0
        self.apiTimeStamps = [0,0,0]

    def __waitForAPIqueryLimit (self):
        currentTime = int(round(time.time() * 1000))
        if currentTime - self.apiTimeStamps [2] < 1000:
            differente = float (1000 - (currentTime-self.apiTimeStamps [2])) / 1000
            if not ('differente' in vars() or 'difference' in globals()):
                print "TROUBLE WITH DIFFERENCE " + str(currentime) + " " + str(self.apiTimeStamps)
            print "Octopart: waiting for " + str(difference * 1000) + " ms"
            os.sleep (difference)
        self.apiTimeStamps [2] = self.apiTimeStamps [1]
        self.apiTimeStamps [1] = self.apiTimeStamps [0]
        self.apiTimeStamps [0] = currentTime

    def getSuppliers(self, partsList):
        if (len(partsList) > 20):
            print "Octopart: too long part list"
        partsQuery = []
        # Prepare query
        for partNumber in partsList:
            partsQuery.append({
                'mpn': partNumber,
                'reference': partNumber
                })

        # Form query
        url = 'http://octopart.com/api/v3/parts/match?queries=%s' \
            % urllib.quote(json.dumps(partsQuery))
        url += '&apikey=' + self.apiKey

        # Wait to avoid excess api rate limit
        self.__waitForAPIqueryLimit()

        # Do API query
        with contextlib.closing(urllib.urlopen(url)) as connection:
            data = connection.read()
        
        response = None
        response = json.loads(data)

        # Process query
        queryResults = []
        for result in response['results']:
            digikeyPN = ""
            mouserPN = ""
            farnellPN = ""
            rsPN = ""
            # Skip not found parts
            if len (result['items']) == 0:
                #print "Octopart: reference " + result['reference'] + " not found"
                continue
            # Get first (most likely) result
            item = result['items'][0]

            for offer in item['offers']:
                #print "Search result for " + result ['reference'] + " " + offer ['seller']['name'] + " sku " + offer ['sku'] + " packaging " + str(offer['packaging'])
                if offer ['seller']['name'] == "Mouser":
                    mouserPN = offer ['sku']
                if offer ['seller']['name'] == "Farnell":
                    farnellPN = offer ['sku']
                if offer ['seller']['name'] == "RS Components":
                    rsPN = offer ['sku']
                if offer ['seller']['name'] == "Digi-Key" and offer['packaging'] == "Cut Tape":
                    digikeyPN = offer ['sku']

            # Store found data
            PN = result ['reference']
            newEntry = {'pn' : PN}
            if mouserPN != "":
                #print "Octopart: found reference " + PN + " on Mouser " + mouserPN 
                newEntry ['Mouser'] = mouserPN
            if farnellPN != "":
                #print "Octopart: found reference " + PN + " on Farnell " + farnellPN 
                newEntry ['Farnell'] = farnellPN
            if rsPN != "":
                #print "Octopart: found reference " + PN + " on RS Components " + rsPN 
                newEntry ['RS'] = rsPN
            if digikeyPN != "":
                #print "Octopart: found reference " + PN + " on Digi-Key " + digikeyPN 
                newEntry ['Digi-Key'] = digikeyPN
            #if mouserPN == farnellPN == rsPN == digikeyPN == "":
                #print "Octopart: reference " + PN + " not found on 4 mayor distributors"
            queryResults.append (newEntry)
        #print queryResults
        return queryResults
"""
Usage example
dataOrigin = Octopart()
dataOrigin.getSuppliers([
    "GRM188R71A224KA01D",
    "GRM188R72A332KA01D",
    "GRM155R60J474KE19J",
    "GRM1555C2AR80BA01D",
    "GRM1555C2AR60BA01D",
    "GRM1885C2A150JA01D",
    "GRM1885C1H331JA01D",
    "GRM1885C2A101JA01D",
    "GRM1885C2A220JA01D",
    "GRM1885C2A180JA01D",
    "GRM1885C2A330JA01D"])
"""
