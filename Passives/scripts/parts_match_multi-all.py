from octopart import Octopart


import sqlite3
import time
import sys
import os
import ConfigParser
import sqlite3


def doQuery(query, tablename):
	global notFound, deleted
	supplierData = dataOrigin.getSuppliers(queries)
	for entry in supplierData:
		if 'Digi-Key' in entry.keys():
			c.execute('''UPDATE ''' + tablename  + ''' SET "supplier 1" = ?, "supplier part number 1"= ? WHERE partNumber = ? ''',
			 ('Digi-Key', entry['Digi-Key'], entry['pn']))
		if 'Mouser' in entry.keys():
			c.execute('''UPDATE ''' + tablename  + ''' SET "supplier 2" = ?, "supplier part number 2"= ? WHERE partNumber = ? ''',
			 ('Mouser', entry['Mouser'], entry['pn']))
		if 'Farnell' in entry.keys():
			c.execute('''UPDATE ''' + tablename  + ''' SET "supplier 3" = ?, "supplier part number 3"= ? WHERE partNumber = ? ''',
			 ('Farnell', entry['Farnell'], entry['pn']))
		if 'RS' in entry.keys():
			c.execute('''UPDATE ''' + tablename  + ''' SET "supplier 4" = ?, "supplier part number 4"= ? WHERE partNumber = ? ''',
			 ('RS-Components', entry['RS'], entry['pn']))
		if not ('Digi-Key' in entry.keys() or 'Mouser' in entry.keys() or 'Farnell' in entry.keys() or 'RS' in entry.keys()):
			#print "Suppliers: " + entry['pn'] + " not found"
			deleted.append (entry['pn'])
			c.execute('''DELETE FROM ''' + tablename  + '''  WHERE partNumber = ? ''', (entry['pn'],))
			notFound = notFound + 1
	db.commit()

# Open config file
config = ConfigParser.ConfigParser()
config.optionxform = str # Avoids converting all options to lowercase
configData = config.read ("sourceConfig.cfg")
if len (configData) != 1:
    raise ValueError, "Failed to open config file sourceConfig.cfg"
outputFolder = config.get ('DataRetrieve', 'outputFolder')
dbPath = os.path.join (os.getcwd(), outputFolder, config.get ('DataRetrieve', 'dbName'))
apiKey = config.get ('SKUmatch','octopartApiKey')

total = 0
notFound = 0
deleted = []

print "Starting octopart parts match..."
dataOrigin = Octopart(apiKey)


db = sqlite3.connect(dbPath)
c = db.cursor()
c.execute("SELECT name FROM sqlite_master WHERE type='table';")

for table in c.fetchall():
	tablename = table [0]
	partial = 0
	
	# Retrieve table items
	c.execute('''SELECT partnumber, "supplier 1", "supplier part number 1", "supplier 2", "supplier part number 2" FROM ''' + tablename)
	all_rows = c.fetchall()
	
	print "Processing table " + tablename + " (" + str(len(all_rows)) + " entries)"
	
	queries = []
	
	# Group rows in 20 entry groups, and trigger the octopart query
	for row in all_rows:
		pn = row[0]
		queries.append(pn)
	
		if len (queries) == 20:
			doQuery (queries, tablename)
			queries = []
			total += 20
			partial += 20
			sys.stdout.write('\r' + "Processed " + str(partial) + " entries...")
			sys.stdout.flush() # important
	# Query remaining items
	if len (queries) > 0:
		total += len (queries)
		partial += len (queries)
		doQuery (queries, tablename)
	print

# Commit changes to DB
db.commit()
db.close()

print
print "Finished!"
print "Retrieved " + str(total) + " component data, which " + str(notFound) + " weren't found"
if len (deleted) > 0:
    print
    print "Parts deleted: "
    for part in deleted:
	    print " -" + part

