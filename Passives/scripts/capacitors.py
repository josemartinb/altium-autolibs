"""
Adds caps from Digikey processed CSV listing
"""
import os
import sys
import datetime
import ConfigParser

import csv
import sqlite3

def resolveDatasheet(series, partNO, supDatasheet):
    """
    Returns indiviual datasheets is there are available, based on current series 
    """
    if series == 'murataGRM':
        link = "http://psearch.en.murata.com/capacitor/product/" + partNO[:-1] + "%23.pdf"
    elif series == 'samsungCL':
        link = supDatasheet
    elif series == 'TDKC':
        link = supDatasheet
    return link

def resolveFootprint (base, index):
    if config.has_option('Footprints', base):
        footprints = [ chunk.strip(None) for chunk in config.get ('Footprints', base).split(",") ]
        if index >= len(footprints):
            return ""
        else:
            return footprints [index]
    else:
        print "No footprints found for " + base
        return ""


series = sys.argv [1]

# Open config file
config = ConfigParser.ConfigParser()
config.optionxform = str # Avoids converting all options to lowercase
configData = config.read ("sourceConfig.cfg")
if len (configData) != 1:
    raise ValueError, "Failed to open config file sourceConfig.cfg"

# Generate component series variables
dataRetrieveURL = config.get (series, 'url')
dbTableName = config.get (series, 'table')
outputFolder = config.get ('DataRetrieve', 'outputFolder')
dataFile = os.path.join (outputFolder, series + ".csv")
dbPath = os.path.join (os.getcwd(), outputFolder, config.get ('DataRetrieve', 'dbName'))

# Retrive data
print "Downloading " + series + " data..."
os.system ("./digikeyRetriever.py \"" + dataRetrieveURL + "\" " + dataFile)

""" Open DB """
db = sqlite3.connect (dbPath)
db.text_factory = str
c = db.cursor ()

# Retrieve the column names
csvFields = dict(config.items('Capacitors'))

# Open downloaded CSV file
inputData = open(dataFile,'r')
csvData = csv.reader(inputData)

# Match indexes of required headers
headers = csvData.next()
csvFieldIndex = {}

for field in csvFields.keys():
    """ In csvFieldIndex the indexes of the relevant headers are stored. The correspondence 
    between the key and the column name is stored in the config file.
    The string is decoded since the escape caracters contained in the column names doubles 
    the slash when are read.
    """
    csvFieldIndex [field] = headers.index (csvFields [field].decode('string_escape'))


# Add data
index = 1
while True:
    # Get next line if there are any remaining
    try:
        part = csvData.next()
    except StopIteration:
        break
    
    # Part properties
    partNO = part [csvFieldIndex['partNO']]
    value = part [csvFieldIndex['value']]
    tolerance = part [csvFieldIndex['tolerance']]
    voltage = part [csvFieldIndex['voltage']]   # Some state 1000V (1kV) or similar
    dielectric = part [csvFieldIndex['dielectric']] # Some state C0G, NP0
    package = part [csvFieldIndex['package']]
    manufacturer = part[csvFieldIndex['manufacturer']]
    manufacturerSeries = part[csvFieldIndex['manufacturerSeries']]
    manufacturerPartNO = partNO
    supplier1 = "Digi-Key"
    supplier1PN = part [csvFieldIndex['supplier1PN']]
    supplier2 = ""
    supplier2PN = ""
    supplier3 = ""
    supplier3PN = ""
    supplier4 = ""
    supplier4PN = ""
    link1Desc = "Datasheet"
    link1URL = resolveDatasheet(series, manufacturerPartNO, part [csvFieldIndex['datasheet']])
    link2Desc = ""
    link2URL = ""
    libraryRef = "Capacitor"
    libraryPath = "Passives.SchLib"
    footprint1ref = resolveFootprint ("CAP" + package.split(" ")[0], 0)
    footprint1path = "Passives.PcbLIb" if (footprint1ref != "") else ""
    footprint2ref = resolveFootprint ("CAP" + package.split(" ")[0], 1)
    footprint2path = "Passives.PcbLIb" if (footprint2ref != "") else ""
    footprint3ref = resolveFootprint ("CAP" + package.split(" ")[0], 2)
    footprint3path = "Passives.PcbLIb" if (footprint3ref != "") else ""
    footprint4ref = resolveFootprint ("CAP" + package.split(" ")[0], 3)
    footprint4path = "Passives.PcbLIb" if (footprint4ref != "") else ""
    height = part [csvFieldIndex['height']].lstrip ("\"").rstrip("\"").split (" ")[-1].lstrip ("(").rstrip (")")  # Save mm only
    description = "Cap " + package + " " + value + " " + tolerance + " " + voltage.split (" ")[0] + " " + dielectric.split (",")[0]
    temperature = part [csvFieldIndex['temperature']].split (" ")[-1]
    specialfeatures = part [csvFieldIndex['specialfeatures']]
    creationDate = str(datetime.date.today())
    
    # Create part tuple
    row = (partNO, value, tolerance, voltage, dielectric, package, manufacturer, manufacturerSeries, manufacturerPartNO, supplier1, supplier1PN, supplier2, supplier2PN, supplier3, supplier3PN, supplier4, supplier4PN, link1Desc, link1URL, link2Desc, link2URL, libraryRef, libraryPath, footprint1ref, footprint1path, footprint2ref, footprint2path, footprint3ref, footprint3path, footprint4ref, footprint4path, height, description, temperature, specialfeatures, creationDate)
    
    """ Add to DB """
    c.execute('insert into ' + dbTableName + ' values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',row)
    index = index + 1

inputData.close()

""" Commit changes"""
db.commit()
db.close()

""" Remove downloaded file """
os.remove (dataFile)

print "Added " + str(index - 1) + " SMD capacitors from " + series + " series"
