import sqlite3
import ConfigParser
import os

# Open config file
config = ConfigParser.ConfigParser()
config.optionxform = str # Avoids converting all options to lowercase
configData = config.read ("sourceConfig.cfg")
if len (configData) != 1:
    raise ValueError, "Failed to open config file sourceConfig.cfg"
outputFolder = config.get ('DataRetrieve', 'outputFolder')
dbPath = os.path.join (os.getcwd(), outputFolder, config.get ('DataRetrieve', 'dbName'))


partsLog = open ("output/PartsLog.csv",'w')

db = sqlite3.connect(dbPath)
c = db.cursor()
c.execute("SELECT name FROM sqlite_master WHERE type='table';")

for table in c.fetchall():
	tablename = table [0]
	c.execute('''SELECT partnumber, "supplier 1", "supplier part number 1", "supplier 2", "supplier part number 2", "manufacturer", "manufacturer part number" FROM ''' + tablename)
	all_rows = c.fetchall()

	for row in all_rows:
		partsLog.write (row[5] + "," + row[6] + "\n")

partsLog.close()
db.close()